
import linear_scaling
import pitch_extraction
import envelope_match_filter
import librosa
import utils
import pyaudio
import time
import numpy as np
import wave
from scipy.io.wavfile import read, write
import io
import os


query_name = ""

def find_similarity_with_songs(signal, sr, user_click_time):
    scores = []
    max_val = max(signal)
    print("signal", signal)
    input_signal = []
    # normalizing signal
    for i in range(len(signal)):
        if i > 100:
            input_signal.append(signal[i] / max_val)

    onsets = envelope_match_filter.find_onsets(input_signal, sr, user_click_time)
    print("my onsets", onsets)
    if len(onsets) == 0:
        return []

    (fundamental_freqs, notes) = pitch_extraction.calculate_freqs_and_notes(input_signal, sr, onsets)
    #print("FREQS:", fundamental_freqs)
    #print("NOTES:", notes)
    
    db_songs_folders = [f for f in os.listdir(utils.database_path) if not f.startswith('.')]
    onsets.append(len(signal))
    onsets_sec = list(map(lambda x: round(x/(sr/1000)), onsets)) 
    #print("onsets_sec", onsets_sec)

    for song_folder in db_songs_folders:
        db_songs = [f for f in os.listdir(utils.database_path + song_folder + '/') if not f.startswith('.')]
        scores.append([song_folder, 0])
        max_score = 0
        for db_song in db_songs:
            #print("piosenka", db_song)
            score = linear_scaling.find_score(fundamental_freqs, notes, onsets_sec, db_song, utils.database_path + song_folder + '/')
            scores[len(scores)-1][1] += score
            max_score += len(notes) 

        scores[len(scores)-1][1] /= max_score

    scores = sorted(scores, key=lambda score: -score[1])

    return scores
    