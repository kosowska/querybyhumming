from scipy.fftpack import fft 
import math
import numpy as np
import utils

def find_min_index(my_fft):
    indx = [0,0,0]
    for i in range(1,len(my_fft)):
        m = np.argmin([my_fft[indx[0]], my_fft[indx[1]], my_fft[indx[2]]])
        if my_fft[indx[m]] < my_fft[i]:
            indx[m] = i
    result_indx = np.argmin([my_fft[indx[0]], my_fft[indx[1]], my_fft[indx[2]]])
    return indx[result_indx]
        

def calculate_freqs(input_signal, sr, onsets):
    leftData = input_signal 

    onsets_len = len(onsets)
    i = 0
    l = 0
    fund_freqs = []
    for i in range(onsets_len):
        if i == onsets_len-1: 
            end = len(input_signal)-1
        else: 
            end = onsets[i+1]-1
       
        my_fft = np.fft.rfft(input_signal[onsets[i] : end])
        freqs = np.fft.rfftfreq(len(input_signal[onsets[i] : end]), 1 / sr)

        min_index = find_min_index(my_fft)
        fund_frequency = freqs[min_index]
        
        fund_freqs.append(fund_frequency)
    
    notes = []
    for n in fund_freqs:
        notes.append(utils.freq2note(n))

    return (fund_freqs, notes)
