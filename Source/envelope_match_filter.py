import librosa
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import librosa.display
import utils
import os

f = [3, 3, 4, 4, -1, -1, -2, -2, -2, -2, -2, -2]
gap = 400
epsilon = 4000
window_size = 700
def A_k(input_signal, window_size, k):
    result = 0

    start = k * gap
    end = k * gap + window_size
    for i in range(start, end):
        if input_signal[i] > result:
            result = input_signal[i]

    return result

def B_k(input_signal, window_size, k):
    a_k = A_k(input_signal, window_size, k)
    result = (a_k / (0.2 + 0.1 * a_k)) ** 0.7

    return result

def C_k(input_signal, window_size, k):
    #liczymy konwolucje
    result = 0
    for i in range(0, 12):
        result += B_k(input_signal,window_size,k-i) * f[i]

    return result

def get_onsets_locations(input_signal, window_size, threshold):
    onsets = []
    k = 0
    while k * gap + window_size < len(input_signal):
        c_k = C_k(input_signal, window_size, k)

        if c_k > threshold:
            onsets.append([k * gap, c_k])
        k += 1

    return onsets

def pick_best_onset_in_epsilon(onsets, epsilon):
    n = len(onsets)
    result = []
    to_delete = set()
    for i in range(0, n):
        for j in range(0, n):
            if abs(onsets[i][0] - onsets[j][0])  > epsilon or i == j:
                continue
            if(onsets[i][1] <= onsets[j][1]):
                to_delete.add(i)
            else:
                to_delete.add(j)

    for i in range(0, n):
        if i not in to_delete:
            if onsets[i][0] <= 0: #CORNER CASE
                continue
            result.append(onsets[i][0]-1200)

    return result


def plot_onsets(input_signal, sr, ons, click_time):
    matplotlib.use('Agg')
    onsets = []
    for i in range(len(ons)):
        onsets.append(ons[i])
    time = np.arange(0, len(input_signal)) / sr
    for i in range(0, len(onsets)):
        onsets[i] = onsets[i] / sr


    fig, ax = plt.subplots()

    for xc in onsets:
        plt.axvline(x=xc, color='k')

    ax.plot(time, input_signal)
    ax.set(xlabel='Time (s)', ylabel='Sound Amplitude')

    plt.savefig(os.getcwd() + '/static/' + str(click_time) + '.png')
    #plt.show()

def find_onsets(input_signal, sr, user_click_time):
    threshold = 6
    onsets = get_onsets_locations(input_signal, window_size, threshold)
    onsets = pick_best_onset_in_epsilon(onsets, epsilon)

    plot_onsets(input_signal, sr, onsets, user_click_time)
    return onsets

