from math import log2, pow
import re 
import numpy as np 
import os
import mido

source_path = os.getcwd()
database_path = os.path.abspath(os.path.join(source_path, os.pardir)) + "/Database/"
A4 = 440
C0 = A4*pow(2, -4.75)
notes_names = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']

def get_notes_from_msg(midofile):
    result = []
    time = 0
    midi_length = 0
    current_time = 0
    for msg in midofile.tracks[0]:
        if not msg.is_meta:
            if msg.type == "note_on":
                midi_length += 1
                current_time += msg.time
                time = round(current_time * 60000 / (mido.tempo2bpm(midofile.tracks[0][0].tempo) * midofile.ticks_per_beat))
                
                (n, octave) = freq2note(MIDI2freq(msg.note))
                note = n + str(octave)
                #print("nowa nuta:", (note, time))
                result.append([note, time, time])
            elif msg.type == "note_off":
                current_time += msg.time
                time = round(current_time * 60000 / (mido.tempo2bpm(midofile.tracks[0][0].tempo) * midofile.ticks_per_beat))
                
                (n, octave) = freq2note(MIDI2freq(msg.note))
                note = n + str(octave)
                for i in range(len(result)-1, 0, -1):
                    if result[i][0] == note:
                        result[i][2] = time
                        break

    return (result, midi_length)

def move_note(note, offset):
    indx = notes_names.index(note[0])
    octave = note[1]
    while offset != 0:
        if offset < 0:
            if indx == 0:
                indx = 11
                octave -= 1
            else:
                indx -= 1
            offset += 1
        if offset > 0:
            if indx == 11:
                indx = 0
                octave += 1
            else:
                indx += 1
            offset -= 1
    return (notes_names[indx], octave)

def calculate_notes_offset(note1, note2):
    #comparing octaves
    result = 0
    if note1[1] < note2[1]: 
        result += 11 - notes_names.index(note1[0])
        result += (note2[1] - note1[1] - 1) * 12
        result += notes_names.index(note2[0]) + 1
    elif note1[1] > note2[1]: 
        result += 11 - notes_names.index(note2[0])
        result += (note1[1] - note2[1] - 1) * 12
        result += notes_names.index(note1[0]) + 1
        result = -result
    else:
        result = notes_names.index(note2[0]) - notes_names.index(note1[0])

    return result

def get_query_full_path(name):
    return query_path + name + ".wav"

def note_name_and_octave(note):
    octave = 0
    i = -1
    ten_exp = 1
    name = ''
    while note[i].isdigit():
        octave += (ord(note[i]) - ord('0')) * ten_exp
        ten_exp *= 10
        i -= 1

    name = note[0:i+1]
    return (name, octave)

def freq2note(freq):
    if freq/C0 == 0:
        return ("UnknownNote0", 0)
    #if freq/C0 == 0 or freq/C0 < 0:
        #print("oj", freq, freq/C0)
    h = round(12*log2(freq/C0))
    octave = h // 12
    n = h % 12
    return (notes_names[n], octave)

def note2freq(note): 
    temp = re.compile("([a-zA-Z]?#?)([0-9]+)") 
    res = temp.match(note).groups() 
    nt = res[0]
    octave = int(res[1])
    idx = notes_names.index(res[0])
    idx = idx + 4 + ((octave - 1) * 12); 

    return round((A4 * 2** ((idx- 49) / 12)),2)

def freq2MIDI(freqs):

    midi = []
    for freq in freqs:
       # if freq/440 == 0: 
         #   m = 0
       # else:
        m = 69 + 12 * np.log2(freq/440)
        midi.append(int(m))

    return midi


def MIDI2freq(m):
    freq = []
    def f(midi):
        g = 2**(1/12)
        return 440*g**(midi-69)
    freq = round(f(m), 2)
                
    return freq
