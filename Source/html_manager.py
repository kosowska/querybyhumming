
def create_table(data):
    # Style 
    table = "\n<head> <style>"
    table +=  " table { border-collapse: collapse; width: 50%; } " 
    table += " th, td { text-align: left; padding: 15px; } "
    table += " tr:nth-child(even){background-color: #f2f2f2} "
    table += " th { background-color: green; color: white; }"
    table += " </style> </head> "
    table += "<table>\n"

    # Create the table's column headers
    header = data[0]
    table += "  <tr>\n"
    for column in header:
        table += "    <th>{0}</th>\n".format(column)
    table += "  </tr>\n"


#customers tr:hover {background-color: #ddd;}
    # Create the table's row data
    for line in data[1:]:
        row = line
        table += "  <tr>\n"
        for column in row:
            table += "    <td>{0}</td>\n".format(column)
        table += "  </tr>\n"

    table += "</table>"
    table += "</div>"
    return table

def create_result_page(data, image_path):
    result = '<link rel="stylesheet" type="text/css" href="querybyhumming/static/style.css">'
    result += '<a href="querybyhumming" class="button">Back</a> \n \n'
    result += '<img src = "' + image_path + '" style="height:400px;" alt ="cfg">\n'
    
    song_information = ''

    if len(data) < 2 or abs(data[1][1] - data[2][1]) < 0.08:
        song_information = 'Unrecognized melody.'
    else:
        song_information = 'Name of the melody is: ' + data[1][0]

    #song_information += create_table(data)

    #return result + song_information + create_table(data)
    return result + '<h1>' + song_information + '</h1>'
