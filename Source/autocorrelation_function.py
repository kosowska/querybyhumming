import librosa
import matplotlib.pyplot as plt
from math import log2, pow
import math
import numpy as np
import scipy.io.wavfile as scp
import utils

def ACF(n, input_signal):
    #funkcja ACF 
    N = len(input_signal)
    result = 0
    
    for i in range(0, int(N-n)):
        result += input_signal[i] * input_signal[i+n]

    result *= 1/(N-n)
    return result

def value_that_maximizes_ACF(input_signal, sr):
    #znajdz wartosc dla ktorej ACF jest najwiekszy 
    result = 0
    max_val = 0.0
    N = len(input_signal)

    for i in range(sr//1000, sr//80):
        val = ACF(i, input_signal)
        if val > max_val:
            max_val = val
            result = i

    return result

def find_pitch_periods(input_signal, sr, onsets):
    #znajdz pitch periods dla kazdego fragmentu od onsetu do onsetu 
    pitch_periods = []
    
    for i in range(0, len(onsets)):
        end = 0
        if i == len(onsets)-1:
            end = len(input_signal)
        else:
            end = onsets[i+1]
        
        fragment = []

        for j in range((int)(onsets[i]), (int)(end)):
            fragment.append(input_signal[j])
        
        pitch_periods.append(value_that_maximizes_ACF(fragment, sr))
    
    return pitch_periods

def calculate_freqs(input_signal, sr, onsets):
    pitch_periods = find_pitch_periods(input_signal, sr, onsets)
    fund_freqs = []
    for period in pitch_periods:
        fund_freqs.append(sr/period)
    notes = []
    for f in fund_freqs:
        notes.append(utils.freq2note(f))

    return (fund_freqs , notes)


