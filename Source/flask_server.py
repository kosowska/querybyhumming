#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, request, render_template, send_from_directory
import query_by_humming as qbh
import html_manager
import werkzeug
from math import floor
from flask import abort
import numpy as np
from os import listdir
from os.path import isfile, join
import os
import time
import atexit

from apscheduler.schedulers.background import BackgroundScheduler

app = Flask(__name__)

def delete_old_onset_graphs():
    cwd = os.getcwd() + '/static/'
    for file in os.listdir(cwd):
        if file.endswith(".png"):
            size = len(file)
            file_name = file[:size - 4]
            file_time_in_ms = int(file_name)
            current_time_in_ms = round(time.time() * 1000)
            if current_time_in_ms - file_time_in_ms > 60 * 1000:
                #print("USUWAM", file_name)
                os.remove(cwd + file)


@app.route("/querybyhumming", methods=['POST', 'GET'])
def index():
    if request.method == "POST":
        print("post halo")
        f = request.files['audio_data'].read()
        sr = int(request.form.get("audio_data_sr", 22050))
        user_click_time = request.form.get("click_time")
        input_signal = np.frombuffer(f, dtype = 'int16')

        if floor(len(input_signal)/sr) < 5 or floor(len(input_signal)/sr) > 20:
            abort(400, "Records length must be in range [5, 20] seconds")

        ranking = [('Song name', 'Similarity(%)')]
        ranking += qbh.find_similarity_with_songs(input_signal, sr, user_click_time)

        cwd = os.getcwd()
        image_path = '/static/' + user_click_time + '.png'
        print("img_path:", image_path)
        new_html = html_manager.create_result_page(ranking, image_path)
        return new_html 
    else:
        print("heeej")
        return render_template("index.html")

@app.route('/querybyhumming/static/<path:path>')
def serve_static(path):
    return send_from_directory('static', path)


if __name__ == "__main__":
    scheduler = BackgroundScheduler()
    scheduler.add_job(func=delete_old_onset_graphs, trigger="interval", seconds=60*60)
    scheduler.start()
    app.run(host="0.0.0.0")

