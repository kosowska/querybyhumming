//webkitURL is deprecated but nevertheless
import {Spinner} from '/querybyhumming/static/js/spin.js';
URL = window.URL || window.webkitURL;

var gumStream;                      //stream from getUserMedia()
var rec;                            //Recorder.js object
var input;                          //MediaStreamAudioSourceNode we'll be recording

// shim for AudioContext when it's not avb. 
var AudioContext = window.AudioContext || window.webkitAudioContext;
var audioContext //audio context to help us record

var recordButton = document.getElementById("recordButton");
var stopButton = document.getElementById("stopButton");

var minutesLabel = document.getElementById("minutes");
var secondsLabel = document.getElementById("seconds");
var totalSeconds = 0;

var timer;


function setTime()
{
    totalSeconds = Math.round(audioContext.currentTime);
    secondsLabel.innerHTML = pad(totalSeconds%60);
    minutesLabel.innerHTML = pad(parseInt(totalSeconds/60));
}

function pad(val)
{
    var valString = val + "";
    if(valString.length < 2)
    {
        return "0" + valString;
    }
    else
    {
        return valString;
    }
}

//SPINNER OPTS

var opts = {
  lines: 13, // The number of lines to draw
  length: 38, // The length of each line
  width: 17, // The line thickness
  radius: 45, // The radius of the inner circle
  scale: 0.17, // Scales overall size of the spinner
  corners: 1, // Corner roundness (0..1)
  speed: 1, // Rounds per second
  rotate: 0, // The rotation offset
  animation: 'spinner-line-fade-quick', // The CSS animation name for the lines
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: '#ffffff', // CSS color or array of colors
  fadeColor: 'transparent', // CSS color or array of colors
  top: '5%', // Top position relative to parent
  left: '8%', // Left position relative to parent
  shadow: '0 0 1px transparent', // Box-shadow for the lines
  zIndex: 2000000000, // The z-index (defaults to 2e9)
  className: 'spinner', // The CSS class to assign to the spinner
  position: 'relative', // Element positioning
};
//

//add events to those 2 buttons
recordButton.addEventListener("click", startRecording);
stopButton.addEventListener("click", stopRecording);



function startRecording() {

    console.log("recordButton clicked");
    
    /*
        Simple constraints object, for more advanced audio features see
        https://addpipe.com/blog/audio-constraints-getusermedia/
    */

    var constraints = { audio: true, video:false }

    /*
        Disable the record button until we get a success or fail from getUserMedia() 
    */

    recordButton.disabled = true;
    stopButton.disabled = false;

    /*
        We're using the standard promise based getUserMedia() 
        https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
    */

    navigator.mediaDevices.getUserMedia(constraints).then(function(stream) {
        console.log("getUserMedia() success, stream created, initializing Recorder.js ...");

        /*
            create an audio context after getUserMedia is called
            sampleRate might change after getUserMedia is called, like it does on macOS when recording through AirPods
            the sampleRate defaults to the one set in your OS for your playback device

        */
        //audioContext = new AudioContext();
        audioContext = new AudioContext({sampleRate: 22050})

        //update the format 
        document.getElementById("formats").innerHTML="Format: 1 channel pcm @ "+audioContext.sampleRate/1000+"kHz"

        /*  assign to gumStream for later use  */
        gumStream = stream;

        /* use the stream */
        input = audioContext.createMediaStreamSource(stream);

        /* 
            Create the Recorder object and configure to record mono sound (1 channel)
            Recording 2 channels  will double the file size
        */
        rec = new Recorder(input,{numChannels:1})

        //start the recording process
        rec.record()
        minutesLabel.innerText = "00";
        secondsLabel.innerHTML = "00";
        totalSeconds = 0;
        timer = setInterval(setTime, 1000);

        console.log("Recording started");

    }).catch(function(err) {
        //enable the record button if getUserMedia() fails
        recordButton.disabled = false;
        stopButton.disabled = true;
    });
}

function stopRecording() {

    clearInterval(timer);
    console.log("stopButton clicked");
    //disable the stop button, enable the record too allow for new recordings
    stopButton.disabled = true;
    recordButton.disabled = false;
    //console.log("SAMPLE RATE:", audioContext.sampleRate);
    
    //tell the recorder to stop the recording
    rec.stop();

    //stop microphone access
    gumStream.getAudioTracks()[0].stop();

    //create the wav blob and pass it on to createDownloadLink
    rec.exportWAV(createDownloadLink);
}
function createDownloadLink(blob) {

    var url = URL.createObjectURL(blob);
    var au = document.createElement('audio');
    var li = document.getElementById('recordingPanel');
    var spinParent = document.createElement('label');
    
   // var recordingContainer = document.getElementById('recordingContainer')
    var waitText = document.createElement('label');
    waitText.style.fontStyle = "italic";
    //var spinner = new Spinner();
    //name of .wav file to use during upload and download (without extendion)
    var filename = new Date().toISOString();

    //add controls to the <audio> element
    au.controls = true;
    au.src = url;

    waitText.innerText = "";
    li.innerHTML = "";
    li.appendChild(au);

    //upload link
    var upload = document.createElement("button");
    upload.href="#";
    upload.innerHTML = "Find similarity";
    upload.addEventListener("click", function(event){
        waitText.innerText = "Please, wait.";
        var spinner = new Spinner(opts).spin(spinParent);
          var xhr=new XMLHttpRequest();
          xhr.onload=function(e) {
              if(this.readyState === 4) {
                document.body.innerHTML =  e.target.responseText;
                console.log("Server returned: ",e.target.responseText);
              }
          };
          var timeInMs = Date.now();
          var fd=new FormData();
          var timeInMs = Date.now();
          fd.append("audio_data", blob, filename);
          fd.append("audio_data_sr", audioContext.sampleRate);
          fd.append("click_time", timeInMs);
          xhr.open("POST","/querybyhumming",true);
          xhr.send(fd);
    })
    li.appendChild(upload)//add the upload link to li
    li.appendChild(waitText);
    li.appendChild(spinParent);
}
