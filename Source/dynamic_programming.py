import librosa
import numpy as np
import matplotlib.pyplot as plt
import librosa.display
from scipy import *
import scipy.fftpack
import math
import mido
import utils


def freqs_2_MIDI(freqs):

    midi = []
    for freq in freqs:
        m = 69 + 12 * np.log2(freq/440)
        midi.append(int(m))

    return midi


def match_score(q, t):
    if q == t: 
        return 2
    else:
        return -2


def compute_align_score(inputMIDI, targetMIDI):
    alignScore = []

    for i in range(len(inputMIDI)):
        alignScore.append([])

        for j in range(len(targetMIDI)):
            
            alignScore[i].append(0)
            if i > 0:
                alignScore[i][j] = max(alignScore[i][j], alignScore[i-1][j])
            if j > 0:
                alignScore[i][j] = max(alignScore[i][j], alignScore[i][j-1])
            if i > 0 and j > 0:
                alignScore[i][j] = max(alignScore[i][j], match_score(inputMIDI[i], targetMIDI[j]) + alignScore[i-1][j-1])
        
    return alignScore[len(inputMIDI)-1][len(targetMIDI)-1]


def find_score(fundamental_freqs, notes,  onsets_sec, song_name, database_path):
    freqs_to_MIDI = utils.freq2MIDI(fundamental_freqs)

    query_notes = []
    for i in range(len(notes)):
        query_notes.append([notes[i],onsets_sec[i]])


    directory = database_path + song_name
    midifile = mido.MidiFile(directory, clip=True)
    (target_notes, midi_length) = utils.get_notes_from_msg(midifile)


    beginning_time = query_notes[0][1]
    for i in range(len(query_notes)):
        query_notes[i][1] -= beginning_time

    beginning_time = target_notes[0][1]
    for i in range(len(target_notes)):
        target_notes[i][1] -= beginning_time
    
    score = compute_align_score(query_notes, target_notes)

    return score

    

