import utils
import mido
import copy

scaling_factors = [0.25, 0.5, 0.75, 1, 1.25, 1.51, 1.75]
epsilon_time = 50

def is_in_interval(elem, interval):
    return elem >= interval[0] and elem <= interval[1]


def calculate_score(scale_factor, queryMIDI, targetMIDI):
    
    interval_begin = 0
    #score = 0
    occurences = []
    score = 0
    offsets_scores = []
    for n in targetMIDI:
        if n[1] == targetMIDI[0][1]:
            offsets_scores.append([utils.calculate_notes_offset(queryMIDI[0][0], utils.note_name_and_octave(n[0])), 0])
        else:
            break

    #print("offets:", offsets_scores)
    for i in range(len(queryMIDI)-1):
        #time_interval = (queryMIDI[i][1] * scale_factor - epsilon_time, queryMIDI[i][1] * scale_factor + epsilon_time)
        scaled_time = queryMIDI[i][1] * scale_factor
        expected_notes = []
        for os in offsets_scores:
            expected_notes.append(utils.move_note(queryMIDI[i][0], os[0]))

        for j in range(len(expected_notes)):
            for tm in targetMIDI:
                #if utils.note_name_and_octave(tm[0]) == expected_notes[j] and is_in_interval(tm[1], time_interval):
                if utils.note_name_and_octave(tm[0]) == expected_notes[j] and is_in_interval(scaled_time, (tm[1], tm[2])):
                    offsets_scores[j][1] += 1
                    break

    score = 0
    for os in offsets_scores:
        score = max(score, os[1])

    return score


def find_score(fundamental_freqs, notes,  onsets_sec, song_name, database_path):
    if len(onsets_sec) == 0: 
        return 0
    freqs_to_MIDI = utils.freq2MIDI(fundamental_freqs)

    query_notes = []
    for i in range(len(notes)):
        query_notes.append([notes[i],onsets_sec[i]])


    directory = database_path + song_name
    midifile = mido.MidiFile(directory, clip=True)
    (target_notes, midi_length) = utils.get_notes_from_msg(midifile)
    
    max_score = 0

    #"normalize" times 
    beginning_time = 0

    if len(query_notes) > 0:
        beginning_time = query_notes[0][1]
    else:
        return 0
    for i in range(len(query_notes)):
        query_notes[i][1] -= beginning_time

    beginning_time = target_notes[0][1]
    for i in range(len(target_notes)):
        target_notes[i][1] -= beginning_time
    
    #print("query_notes:", query_notes)
    #print("target midi:", target_notes)
    for scale_factor in scaling_factors:
        #score = calculate_score(scale_factor, queryMIDI, targetMIDI)
        score = calculate_score(scale_factor, query_notes, target_notes)
        max_score = max(max_score, score)

    return max_score




