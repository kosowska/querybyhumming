from __future__ import division

import numpy as np
import utils

    

def find_max_index(my_fft):
    max_indx = 0
    for i in range(1,len(my_fft)):
        if my_fft[max_indx] < my_fft[i]:
            max_indx = i

    return max_indx
    

def calculate_freqs(input_signal, sr, onsets):
    onsets_len = len(onsets)
    fund_freqs = []

    fmax = 1000 #Hz
    fmin = 200  #Hz

    for i in range(onsets_len):
        if i == onsets_len-1: 
            end = len(input_signal)
        else: 
            end = onsets[i+1]-1

        signal_for_onset = input_signal[onsets[i] : end]
  
        freqs = np.fft.rfftfreq(len(signal_for_onset), d=1/sr)
        log_on_fft = np.log(np.abs(np.fft.rfft(signal_for_onset)))
        cepstrum = np.fft.rfft(log_on_fft)
        quefrencies= np.fft.rfftfreq(log_on_fft.size, freqs[1] - freqs[0])
        cepstrum_len = len(cepstrum)
        peaks = np.zeros(cepstrum_len)

        for i in range(1, cepstrum_len-1):
            y1 = cepstrum[i-1]
            y2 = cepstrum[i]
            y3 = cepstrum[i+1]
            if (y2 > y1 and y2 >= y3):
                peaks[i] = cepstrum[i]
        
        valid = (quefrencies > 1/fmax) & (quefrencies <= 1/fmin)
        # Maximal value amond peaks in cepstrum data
        max_indx = find_max_index(peaks[valid])
        fundamental_frequency = 1/quefrencies[valid][max_indx]
        fund_freqs.append(fundamental_frequency)
    
    notes = []
    for f in fund_freqs:
        notes.append(utils.freq2note(f))

    return (fund_freqs , notes)

