import third_largest_fft
import harmonic_product
import cepstral_method
import autocorrelation_function

from collections import Counter

def calculate_freqs_and_notes(input_signal, sr, onsets):

    (third_freqs, third_notes) = third_largest_fft.calculate_freqs(input_signal, sr,  onsets)
    print("third:", third_freqs)
    (harmonic_freqs, harmonic_notes) =  harmonic_product.calculate_freqs(input_signal, sr, onsets)
    print("harmonic:", harmonic_freqs)
    (cepstral_freqs, cepstral_notes) = cepstral_method.calculate_freqs(input_signal, sr, onsets)
    print("cepstral", cepstral_freqs)
    #(autocorrelation_freqs, autocorrelation_notes) = autocorrelation_function.calculate_freqs(input_signal, sr, onsets)
    
    preferred_notes = cepstral_notes
    
    notes = []
    freqs = []
    for i in range(len(harmonic_notes)):

        candidate_notes = [third_notes[i], harmonic_notes[i], cepstral_notes[i]]
        b = Counter(candidate_notes)
        n = max(b)
        if b[n] == 1: 
            n = preferred_notes[i] 
        elif b[n] == 2 and b[preferred_notes[i]] == 2:  
            n = preferred_notes[i]

        notes.append(n)
    
    return (freqs,notes)
    

    
