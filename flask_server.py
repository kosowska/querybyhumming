#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, request, render_template, send_from_directory
import query_by_humming as qbh
import html_manager
import werkzeug
from math import floor
from flask import abort
import numpy as np


app = Flask(__name__)

'''
@app.errorhandler(werkzeug.exceptions.BadRequest)
def handle_bad_request(e):
    return 'bad request!', 400

@app.errorhandler(werkzeug.exceptions.NotFound)
def handle_not_found(e):
    return 'not found!', 404

@app.errorhandler(werkzeug.exceptions.InternalServerError)
def handle_internal_error(e):
    return 'App has failed!', 500
'''


@app.route("/querybyhumming", methods=['POST', 'GET'])
def index():
    if request.method == "POST":
        f = request.files['audio_data'].read()
        sr = int(request.form.get("audio_data_sr", 44100))
        input_signal = np.frombuffer(f, dtype = 'int16')

        if floor(len(input_signal)/sr) < 3 or floor(len(input_signal)/sr) > 15:
            abort(400, "Records length must be in range [3, 15] seconds")

        ranking = [('Song name', 'Similarity(%)')]
        ranking += qbh.find_similarity_with_songs(input_signal, sr)
        new_html = html_manager.create_result_page(ranking)
        return new_html
    else:
        return render_template("index.html")

@app.route('/querybyhumming/static/<path:path>')
def serve_static(path):
    return send_from_directory('static', path)


if __name__ == "__main__":
    app.run(host="0.0.0.0")
